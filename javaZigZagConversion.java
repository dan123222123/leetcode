public class javaZigZagConversion {
    public String convert(String s, int numRows) {
        int row = numRows - 1, cols = s.length()/2;
        String[][] zigzag = new String[numRows][s.length()/2 + 1];
        int charCount = 0, rowCount = 0, colCount = 0;
        if(numRows == 1) return s;
        while(charCount < s.length()){
            String currentLetter = Character.toString(s.charAt(charCount));
            if(rowCount == row){
                while((charCount < s.length() - 1) && rowCount > 0){
                    zigzag[rowCount][colCount] = currentLetter;
                    charCount++;
                    colCount++;
                    currentLetter = Character.toString(s.charAt(charCount));
                    rowCount--;
                }
            }
            zigzag[rowCount][colCount] = currentLetter;
            rowCount++;
            charCount++;
        }
        String retStr = "";
        for(int i = 0; i < zigzag.length; i++){
            for(int j = 0; j < zigzag[i].length;j++){
                if(zigzag[i][j] != null){
                    retStr += zigzag[i][j];
                }
            }
        }
        return retStr;
    }
}