/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class javaAddTwoNumbers {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode first = new ListNode(0);
        ListNode iterate = first;
        ListNode l1Temp = l1;
        ListNode l2Temp = l2;
        while(l1Temp != null || l2Temp != null){
            if(l1Temp != null){
                iterate.val += l1Temp.val;
                l1Temp = l1Temp.next;
            }
            if(l2Temp != null){
                iterate.val += l2Temp.val;
                l2Temp = l2Temp.next;
            }
            if(iterate.val >= 10){
                    iterate.val -= 10;
                    iterate.next = new ListNode(1);
            }
            if(iterate.next == null && (l1Temp != null || l2Temp != null)){
                iterate.next = new ListNode(0);
                
            }
                iterate = iterate.next;
        }
        return first;
    }
}